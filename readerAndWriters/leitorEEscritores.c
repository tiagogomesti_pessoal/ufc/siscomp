#include "lib.h"





int main(void){
	int i;	
	char str[30];
	pthread_t readerThread[N_READERS];
	pthread_t writerThread[N_WRITERS];
	pthread_t monitorThread;
	int nReaders[N_READERS];
	int nWriter[N_WRITERS];

	sem_init(&mutex,0,1); // inicialização do mutex com 1
	sem_init(&db,0,1); // inicialização da data base com 1
	rc = 0; // numero de processos lendo ou querendo ler
	
	i=0;
	while(i<N_READERS){
		nReaders[i] = i;
		pthread_create(&readerThread[i], NULL, reader, &nReaders[i]);
		i++;		
	}
	
	i=0;
	while(i<N_WRITERS){
		nWriter[i] = i;
		pthread_create(&writerThread[i], NULL, writter, &nWriter[i]);
		i++;
	}
	
	i=0;
	while(i<N_READERS){
		pthread_join(readerThread[i], NULL);
		i++;
	}

	i=0;
	while (i<N_WRITERS){
		pthread_join(writerThread[i], NULL);
		i++;
	}


	return 1;
}





























