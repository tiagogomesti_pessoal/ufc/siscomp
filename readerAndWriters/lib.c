#include "lib.h"

void read_data_base(int *n){
		printf("leitor %d lendo\n\n",*n);
		sleep(rand()%2);
}
void use_data_read(int *n){
		printf("leitor %d usando os dados\n\n",*n);
		sleep(rand()%2);
}



void* reader(void* j){
	int* n = (int*) j;
	
	while(TRUE){
		sem_wait(&mutex);
		rc = rc + 1;
		if(rc == 1)
			sem_wait(&db);
		sem_post(&mutex);
		read_data_base(n);
		sem_wait(&mutex);
		rc = rc - 1;
		if(rc == 0)
			sem_post(&db);
		sem_post(&mutex);
		use_data_read(n);
	}
}


void think_up_data(int *n){
		printf("escritor %d gerando dados\n\n",*n);
		sleep(rand()%2);

}
void write_data_base(int *n){
		printf("escritor %d escrevendo\n\n",*n);
		sleep(rand()%2);
}
void* writter(void* j){
	int* n = (int*) j;	
	
	while(TRUE){
		think_up_data(n);
		sem_wait(&db);
		write_data_base(n);
		sem_post(&db);
	}
}	
	
	




