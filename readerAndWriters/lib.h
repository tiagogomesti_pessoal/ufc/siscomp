#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>
#include <string.h>
#define TRUE 1
#define N_READERS 4
#define N_WRITERS 1



sem_t mutex;
sem_t db;
int rc;
FILE *fp;

void read_data_base(int *n);
void use_data_read(int *n);
void *reader(void* j);

void think_up_data(int *n);
void write_data_base(int *n);
void *writter(void* j);


