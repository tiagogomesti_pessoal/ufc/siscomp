#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <sys/types.h>
#include <unistd.h>
#define TRUE 1

struct command{
  char *path;
  char **parameters;  
};
typedef struct command command;


int read_command(command *shell);
void desaloca (command *shell, int n);

