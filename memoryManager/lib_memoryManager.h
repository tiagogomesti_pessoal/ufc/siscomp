#ifndef LIB_MEMORY_MANAGER
#define LIB_MEMORY_MANAGER

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>

#define TRUE 1
#define FALSE 0

void chooseMemoryMap (int *i, void (**memoryMap)(void* n) );
void* mpBitsMaps(void* j);
void* mpSingleList(void* j);
void* mpDoubleList(void* j);
























#endif
